/*
 * Name: Daniele Huang
 *
 * Login ID: cs8sbaoj
 *
 * Date: 04/05/2016
 * */

public class FunWithIntArrays {

 //method that returns the int in the array with the highest value
 //@param array int array to be scanned
 //@return int from the array with the highest value
 public static int findMax(int[] array) {
  //short circuit protects null access
  if (array == null || array.length == 0)
   return -1;

  int max = array[0];

  for (int i = 0; i < array.length; i++) {
   if (array[i] > max) {
    max = array[i];
   }
  }

  return max;
 }

 //method that returs the int in the array with the lowest value
 //@param array int array to be scanned
 //@return int from the array with the lowest value
 public static int findMin(int[] array) {
  //short circuit protects null access
  if (array == null || array.length == 0)
   return -1;

   int  min = array[0];

  for (int i = 0; i < array.length; i++) {
   if (array[i] < min) {
    min = i;
   }
  }

  return min;
 }


 //method that creates a deep copy of the passed array
 //@param array the array to be copied
 //@return the copy of the array
 public static int[] arrayCopy(int[] array) {
  if (array == null)
   return null;

  int[] result = new int[array.length];

  for (int i = 0; i < array.length; i++) {
   result[i] = array[i];
  }
  return result;
 }

 //method that prints all the elements in the array
 //@param array the array to be printed
 public static void printArray(int[] array) {
  if (array == null)
   return;

  for (int i = 0; i < array.length; i++) {
   System.out.print(array[i] + ", ");
  }

  System.out.println();
 }

 //method that returns a sorted copy of the array
 //@param array the array to be sorted
 //@return a sorted copy of the passed array
 public static int[] arraySort(int[] array) {
  if (array == null)
   return null;

  int[] result = new int[array.length];
  result=arrayCopy(array);

  for (int i = 0; i < array.length; i++) {
   for (int j = 0; j < array.length - 1; j++) {
    if (result[j] > result[j + 1]) {
    // swapping array[j] and array[j+1]
     int temp = result[ j ];
     result[ j ] = result[ j + 1];
     result[ j + 1 ] = temp;
    }
   }
  }
  return result;
  
 }
}
