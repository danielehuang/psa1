// Keep these two lines.  They are what tell Java to include the
// classes you need for working with files.
// You might get warnings about them at first.  That's OK, just
// ignore the warnings.  They should go away as you complete your code.
import java.io.*;
import java.util.*;

/** Your header comment goes here.
 *
 * Name : Daniele Huang
 *
 * Login ID: cs8sbaoj
 *
 * Date : 04/05/2016
 * */
public class Caesar {
 // Complete the methods below.  Be sure to add header
 // comments for each. You may (and should) also write additional
 // helper methods.  Be sure to make the helper methods private and include
 // header comments for each.

 // Although you will not be graded on style this week, you should follow
 // these basic style guidelines nonetheless.   You will be graded on this
 // in weeks to come, so start to practice now.

 // Use proper indenting: Indent each block of code (e.g., method body,
 //   loop body.  Line up the lines in the block so that they are all
 //   indented to the same degree.  See examples of this in the book
 //   and in the code below.
 // Use descriptive variable names: The names of your variables should
 //   describe the data they hold.  Almost always, your variable names
 //   should be words (or abbreviations), not single letters.
 // Write short methods: Break your methods up into submethods if they
 //   are getting too complicated or long.  Generally your methods
 //   shouldn't get too much longer than about 20 lines of code (give or take)
 // Write short lines: Each line of code should be no longer than 80
 //   characters, so it can fit in a reasonable size window.  There's a
 //   column number in both vim and emacs.
 //
 // We'll start with these, as these are the most important.  We may add
 // to this list later in the term, but if you do all of the above you're
 // in good shape.


/* Static Method that encrypts a string according to the rotation given, if the
 * passed string is null then the method will return a null string.
  * @param s the string to be encrypted
  * @param rotation the number of rotation that will be used for encrypting
  * @return a string containing the encrypted version of the passed string
  * */
 public static String encrypt(String s, int rotation) {
   if(s==null)
      return null;

   char[] wordToEncrypt = s.toCharArray();
   char[] result = wordToEncrypt;


//for loop that goes through each character in the string in order to encrypt
//them one by one.

   for(int index=0;index<result.length;index++){
     result[index]= letterOperation(result[index],rotation);
   }
  String encryptedWord = new String(result);
  return encryptedWord;
 }


// Static Method to decrypt a string according to the given value of rotation.
// The method will not run if the passed string is null and will return a null
// string.
// @param s the string to decrypt;
// @param rotation the value of rotation to decrypt the string
// @return a string with the decrypted message of the passed string

 public static String decrypt(String s, int rotation) {
   if(s==null)
      return null;
 
   char[] wordToDecrypt = s.toCharArray();
   char[] result = wordToDecrypt;

// for loop that goes through each character of the string in order to
// decrypt them one by one.

   for(int index=0; index< result.length; index++){

// a complimentary rotation is applied so that encrypting with that rotation
// is actually equal to decrypting with the complimentary rotation.
      result[index]=letterOperation(result[index],(26-rotation));
   }
  String decryptedWord = new String(result);
  return decryptedWord;
 }


// Private static method that encrypts or decrypts the passed alphabetic
// letter according to the passed rotation value
// @param a the character to be encrypted or decrypted
// @param rotation value of rotation 
// @return the encrypted/decrypted character 

 private static char letterOperation(char a, int rotation) {  
// variable that takes care of the cases when the passed rotation value
// is greater than 26.
   int multRotation = (int)(rotation/26);
   if(rotation > 26){
      rotation = rotation - multRotation*26;
   }
   else if(rotation <0){
//multRotation-1 makes sure that the rotation is in fact a positive integer
      rotation = rotation - (multRotation-1)*26;
   }
//if statement to separate uppercase letters from lowercase letters to
//facilitate the following encryption/decryption.
   if(Character.isUpperCase(a)){
      a = (char)((int)a+rotation);
//if statement that checks that a is still an uppercase letter
      if( (int)a > 90){
//lines that "stick Z beside A", so that the uppercase letter once encrypted
//is still an uppercase letter
         int unitsOff = (int)a-90;
         a = (char)(unitsOff+64);
      }
   }
   else if(Character.isLowerCase(a)){
      a =(char)((int)a+rotation);
//if statement that checks that a is still a lowercase letter
      if((int)a>122){
//lines that "stick z beside a", so that the lowercase once encrypted is
//still a lowercase letter
         int unitsOff = (int)a-122;
         a = (char)(96+unitsOff);
      }
    }
 
  return a;
 }
}
